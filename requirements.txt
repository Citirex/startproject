Django==1.9.7
argparse==1.2.1
httplib2==0.9.2
oauth2==1.9.0.post1
psycopg2==2.6.1
wsgiref==0.1.2
simplejson==3.8.2
google_api_python_client==1.5.1
oauth2client==2.2.0
# For tests
selenium==2.53.5
