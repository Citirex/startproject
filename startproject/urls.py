from django.conf.urls import url, include
import social_auth

urlpatterns = [
    url(r'^', include('social_auth.urls')),
]
