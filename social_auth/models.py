from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Profile(models.Model):
    FACEBOOK = 'fb'
    GOOGLE = 'gg'
    TWITTER = 'tw'
    AUTH_PROVIDER_CHOICES = (
        (FACEBOOK, 'Facebook'),
        (TWITTER, 'Twitter'),
        (GOOGLE, 'Google'),
    )
    MALE = 'm'
    FEMALE = 'f'
    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    auth_provider = models.CharField(
        max_length=2,
        choices=AUTH_PROVIDER_CHOICES,
        help_text=_('Service which was used for registration'),
        blank=True,
        null=True
    )

    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES,
        help_text=_("User's gender"),
        blank=True,
        null=True
    )
    user = models.OneToOneField(User, related_name='profile')
    tw_uid = models.CharField(help_text=_("User's Twitter ID"), blank=True, null=True, max_length=50)
    fb_uid = models.CharField(help_text=_("User's Facebook ID"), blank=True, null=True, max_length=50)

