from django.test import TestCase
from selenium import webdriver

from test_credentials import test_data

import unittest


class TemplateTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()

    def tearDown(self):
        self.driver.close()

    def test_login_page(self):
        """
        Tests if there is 3 buttons on base page template, that redirect you to pages:
        fb - login/facebook
        tw - login/twitter
        google - login/google
        """
        self.skipTest('Not implemented')



    def test_success_page(self):
        """
        Test if there is congratulations page with hello, <%username%> in it
        """
        self.skipTest('Not implemented')


class SocialLoginTest(TestCase):
    def test_facebook_login(self):
        """
        Selenium test...
        Test user (test credentials will be provided)
        should be able to login with his data and after giving access, get to the success page
        If he is male - he should be authorized, otherwise - not authorized to view the *SECRET*
        """
        driver = webdriver.Firefox()
        driver.get("http://citirex.com.ua")
        element = driver.find_element_by_xpath("/html/body/div[1]/div[2]/form/a[2]")
        element.click()
        email = driver.find_element_by_id("email")
        email.send_keys(test_data["facebook"].keys()[0])
        password = driver.find_element_by_id("pass")
        password.send_keys(test_data["facebook"].values()[0])
        email.submit()
        try:
            assert "Access granted" in driver.page_source
        except AssertionError:
            self.fail("No access page")
        else:
            self.skipTest("Test PASS")
        finally:
            driver.close()
        
    def test_google_login(self):
        """
        Selenium test...
        Test user (test credentials will be provided)
        should be able to login with his data and after giving access, get to the success page
        If he is male - he should be authorized, otherwise - not authorized to view the *SECRET*
        """
        self.skipTest('Not implemented')
        
    def test_twitter_login(self):
        """
        Selenium test...
        Test user (test credentials will be provided)
        should be able to login with his data and after giving access, get to the success page
        If he is male - he should be authorized, otherwise - not authorized to view the *SECRET*
        """
        self.skipTest('Not implemented')
       

class DatabaseTest(TestCase):
    def test_database_backup(self):
        """
        Tests if database back's up after running corresponding shell script
        """
        self.skipTest('Not implemented')

    def test_database_restores(self):
        """
        Tests if database restores after running sorresponding shell script
        """
        self.skipTest('Not implemented')
