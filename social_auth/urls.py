from django.conf.urls import url
from . import views
from django.contrib.auth.views import logout

urlpatterns = [
    url(r'^connect/(?P<social_slug>(twitter|facebook|google))$', views.connect_social, name='auth_social'),
    url(r'^connect/(?P<social_slug>(twitter|facebook|google))/complete/$', views.connect_complete),
    url(r'^$', views.index, name='index'),
    url(r'^success/$', views.auth_success, name='auth_success'),
    url(r'^logout/$', logout, {'next_page': '/'}, name="logout"),
    url(r'^api/(?P<social_slug>(twitter|facebook|google))/get_link/$', views.api_get_link, name='api_get_link'),
    url(r'^api/connect/(?P<social_slug>(twitter|facebook|google))/complete/$', views.api_connect_complete,
        name='api_auth_social')
]
