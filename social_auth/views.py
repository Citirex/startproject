from django.shortcuts import render, redirect

import oauth2 as oauth

import urllib
import urlparse
import simplejson
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponseNotFound, JsonResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from social_auth.models import Profile
from django.utils.translation import ugettext as _
import os
from oauth2client.contrib import xsrfutil
from oauth2client.client import flow_from_clientsecrets
from oauth2client.contrib.django_orm import Storage
import httplib2
from googleapiclient.discovery import build
import logging
from random import choice
from string import ascii_lowercase, digits

SECRET = "Python is your choice for the web development."
SCOPES = 'https://mail.google.com/'

# Twitter configuration
TWITTER_SERVER = 'api.twitter.com'
TWITTER_REQUEST_TOKEN_URL = 'https://%s/oauth/request_token' % TWITTER_SERVER
TWITTER_ACCESS_TOKEN_URL = 'https://%s/oauth/access_token' % TWITTER_SERVER
TWITTER_AUTHORIZATION_URL = 'https://%s/oauth/authenticate' % TWITTER_SERVER
TWITTER_CHECK_AUTH = 'https://twitter.com/account/verify_credentials.json'

# Facebook configuration
FACEBOOK_ACCESS_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token'
FACEBOOK_REQUEST_TOKEN_URL = 'https://www.facebook.com/dialog/oauth'
FACEBOOK_CHECK_AUTH = 'https://graph.facebook.com/'

# Google configuration
GOOGLE_CLIENT_SECRETS = os.path.join(os.getcwd(), 'google_client_secrets.json')
GOOGLE_FLOW = flow_from_clientsecrets(
        GOOGLE_CLIENT_SECRETS,
        scope='https://www.googleapis.com/auth/plus.me https://mail.google.com/',
        redirect_uri='http://citirex.com.ua/connect/google/complete/')
GOOGLE_FLOW_API = flow_from_clientsecrets(
    GOOGLE_CLIENT_SECRETS,
    scope='https://www.googleapis.com/auth/plus.me https://mail.google.com/',
    redirect_uri='http://citirex.com.ua/api/connect/google/complete/')


def connect_social(request, social_slug):
    if social_slug == 'twitter':
        consumer = oauth.Consumer(key=settings.TWITTER_CONSUMER_KEY, secret=settings.TWITTER_CONSUMER_SECRET)
        callback_url = 'http://' + request.META['HTTP_HOST'] + '/connect/twitter/complete/'
        client = oauth.Client(consumer)
        resp, content = client.request(TWITTER_REQUEST_TOKEN_URL, "POST",
                                       body=urllib.urlencode({'oauth_callback':callback_url}))

        request_token = dict(urlparse.parse_qsl(content))
        roauth_token = request_token['oauth_token']
        roauth_token_secret = request_token['oauth_token_secret']
        request.session['roauth_token'] = roauth_token
        request.session['roauth_token_secret'] = roauth_token_secret

        auth_url_params = {
            'oauth_token': roauth_token,
            'oauth_callback': callback_url
        }
        return HttpResponseRedirect(TWITTER_AUTHORIZATION_URL + '?' + urllib.urlencode(auth_url_params))
    elif social_slug == 'facebook':
        callback_url = 'http://' + request.META['HTTP_HOST'] + '/connect/facebook/complete/'
        return HttpResponseRedirect(FACEBOOK_REQUEST_TOKEN_URL + '?client_id=%s&redirect_uri=%s&scope=%s'
                                    % (settings.FACEBOOK_APP_ID,  urllib.quote_plus(callback_url), 'email'))
    elif social_slug == 'google':
        if request.user.is_anonymous():
            authorize_url = GOOGLE_FLOW.step1_get_authorize_url()
            return HttpResponseRedirect(authorize_url)
        else:
            return redirect('auth_success')

    else:
        return HttpResponseNotFound(_('Wrong platform.'))


def connect_complete(request, social_slug):
    if social_slug == 'twitter':
        connect_twitter(request)
    elif social_slug == 'facebook':
        connect_facebook(request)
    elif social_slug == 'google':
        connect_google(request)
    return redirect('auth_success')


def connect_twitter(request):
    consumer = oauth.Consumer(key=settings.TWITTER_CONSUMER_KEY, secret=settings.TWITTER_CONSUMER_SECRET)
    oauth_verifier = request.GET['oauth_verifier']
    try:
        token = oauth.Token(request.session.get('roauth_token', None), request.session.get('roauth_token_secret', None))
    except:
        return HttpResponseBadRequest()
    token.set_verifier(oauth_verifier)
    client = oauth.Client(consumer, token)

    resp, content = client.request(TWITTER_ACCESS_TOKEN_URL, "POST")
    access_token = dict(urlparse.parse_qsl(content))

    del request.session['roauth_token']
    del request.session['roauth_token_secret']

    userid = access_token['user_id']
    screenname = access_token['screen_name']

    try:
        profile = Profile.objects.get(tw_uid=userid)
    except Profile.DoesNotExist:
        new_user = User.objects.create_user('%s_twitter' % screenname, screenname + '@twitter.com', userid)
        new_user.save()
        profile = Profile(user=new_user, tw_uid=userid)
        profile.auth_provider = Profile.TWITTER
        profile.save()
    user = authenticate(username=profile.user.username, password=profile.tw_uid)
    login(request, user)


def connect_facebook(request):
    code = request.GET.get('code')
    consumer = oauth.Consumer(key=settings.FACEBOOK_APP_ID, secret=settings.FACEBOOK_APP_SECRET)
    client = oauth.Client(consumer)
    redirect_uri = 'http://' + request.META['HTTP_HOST'] + '/connect/facebook/complete/'
    request_url = FACEBOOK_ACCESS_TOKEN_URL + \
        '?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s' %\
        (settings.FACEBOOK_APP_ID, redirect_uri, settings.FACEBOOK_APP_SECRET, code)
    resp, content = client.request(request_url, 'GET')
    access_token = dict(urlparse.parse_qsl(content))['access_token']
    request_url = FACEBOOK_CHECK_AUTH + '?access_token=%s' % access_token

    if resp['status'] == '200':
        user_data = get_fb_user_data(access_token)
        userid = user_data['id']
        try:
            profile = Profile.objects.get(fb_uid=userid)
        except:
            name = user_data['name']
            new_user = User.objects.create_user('%s_facebook' % name, user_data['email'], userid)
            new_user.save()
            profile = Profile(user=new_user, fb_uid=userid)
            profile.gender = user_data['gender'][0].lower()
            profile.auth_provider = Profile.FACEBOOK
            profile.save()
        user = authenticate(username=profile.user.username, password=profile.fb_uid)
        login(request, user)


def get_fb_user_data(access_token):
    url = FACEBOOK_CHECK_AUTH + 'v2.6/me?fields=id,name,gender,email,first_name,last_name&access_token=%s' % \
          access_token
    consumer = oauth.Consumer(key=settings.FACEBOOK_APP_ID, secret=settings.FACEBOOK_APP_SECRET)
    client = oauth.Client(consumer)
    resp, content = client.request(url, 'GET')
    content_dict = simplejson.loads(content)
    return content_dict


def connect_google(request):
    if request.user.is_anonymous():
        the_user = User()
    else:
        the_user = request.user
    try:
        credential = GOOGLE_FLOW.step2_exchange(request.GET)
    except:
        return HttpResponseBadRequest()
    http = httplib2.Http()
    http = credential.authorize(http)

    service1 = build('plus', 'v1', http=http)
    service2 = build('gmail', 'v1', http=http)
    name = service1.people().get(userId='me', fields='name').execute().pop('name', None)
    the_user_username = name['familyName'] + '_' + name['givenName']
    same_users = User.objects.filter(username=the_user_username)
    if same_users:
        the_user = same_users[0]
        profile = Profile.objects.get(user=the_user)
    else:
        profile = Profile()

    email = service2.users().getProfile(userId='me', fields='emailAddress').execute()
    the_user.email = email[u'emailAddress']
    the_user.set_password(the_user.email)
    gender = service1.people().get(userId='me', fields='gender').execute()
    name = service1.people().get(userId='me', fields='name').execute().pop('name', None)
    if name:
        the_user.username = name['familyName'] + '_' + name['givenName']
    else:
        the_user.username = the_user.email
    the_user.save()
    gender = gender.pop('gender', None)
    if gender:
        gender = gender[0]
    profile.gender = gender
    profile.user = the_user
    profile.auth_provider = Profile.GOOGLE

    profile.save()
    the_user = authenticate(username=the_user.username, password=the_user.email)
    login(request, the_user)


def get_access_token(request):
    """ Checks if a certain access token for fb does exist in the session """
    if request.session.has_key('access_token'):
        return request.session['access_token']
    consumer = oauth.Consumer(key=settings.FACEBOOK_APP_ID, secret=settings.FACEBOOK_APP_SECRET)
    client = oauth.Client(consumer)
    request_url = 'https://graph.facebook.com/oauth/access_token' \
                  '?client_id=%s&client_secret=%s&grant_type=client_credentials' %\
                  (settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET)
    resp, content = client.request(request_url, 'GET')
    access_token = dict(urlparse.parse_qsl(content))['access_token']
    if resp['status'] == '200':
        request.session['access_token'] = access_token
    return access_token


def index(request):
    return render(request, 'base.html')


@login_required
def auth_success(request):
    u = request.user
    profile = Profile.objects.get(user=u)
    if profile.gender == Profile.MALE:
        return render(request, 'congratulations.html', {'secret': SECRET})
    else:
        return render(request, 'congratulations.html')


def api_get_link(request, social_slug):
    if social_slug == 'twitter':
        consumer = oauth.Consumer(key=settings.TWITTER_CONSUMER_KEY, secret=settings.TWITTER_CONSUMER_SECRET)
        callback_url = 'http://' + request.META['HTTP_HOST'] + '/api/connect/twitter/complete/'
        client = oauth.Client(consumer)
        resp, content = client.request(TWITTER_REQUEST_TOKEN_URL, "POST",
                                       body=urllib.urlencode({'oauth_callback': callback_url}))

        request_token = dict(urlparse.parse_qsl(content))
        roauth_token = request_token['oauth_token']
        roauth_token_secret = request_token['oauth_token_secret']
        request.session['roauth_token'] = roauth_token
        request.session['roauth_token_secret'] = roauth_token_secret

        auth_url_params = {
            'oauth_token': roauth_token,
            'oauth_callback': callback_url
        }
        url = TWITTER_AUTHORIZATION_URL + '?' + urllib.urlencode(auth_url_params)
        return JsonResponse({'url' : url})
    elif social_slug == 'facebook':
        callback_url = 'http://' + request.META['HTTP_HOST'] + '/api/connect/facebook/complete/'
        url = FACEBOOK_REQUEST_TOKEN_URL + '?client_id=%s&redirect_uri=%s&scope=%s'\
                                           % (settings.FACEBOOK_APP_ID, urllib.quote_plus(callback_url), 'email')
        return JsonResponse({'url' : url})
    elif social_slug == 'google':
        authorize_url = GOOGLE_FLOW_API.step1_get_authorize_url()
        return JsonResponse({'url': authorize_url})

    else:
        return JsonResponse({'Wrong platform'})


def api_connect_complete(request, social_slug):
    """ Checks what platform does the connecting of social platform comes from """
    if social_slug == 'twitter':
        """ Gets the profile info and access token from twitter """

        consumer = oauth.Consumer(key=settings.TWITTER_CONSUMER_KEY, secret=settings.TWITTER_CONSUMER_SECRET)
        try:
            oauth_verifier = request.GET['oauth_verifier']
        except:
            return JsonResponse({'code': '402'})

        try:
            token = oauth.Token(request.session.get('roauth_token', None), request.session.get('roauth_token_secret', None))
            token.set_verifier(oauth_verifier)
        except:
            return JsonResponse({'code': '402'})
        client = oauth.Client(consumer, token)

        resp, content = client.request(TWITTER_ACCESS_TOKEN_URL, "POST")
        access_token = dict(urlparse.parse_qsl(content))

        del request.session['roauth_token']
        del request.session['roauth_token_secret']
        try:
            userid = access_token['user_id']
            screenname = access_token['screen_name']
        except:
            return JsonResponse({'code': '402'})

        try:
            profile = Profile.objects.get(tw_uid=userid)
        except Profile.DoesNotExist:
            new_user = User.objects.create_user('%s_twitter' % screenname, screenname + '@twitter.com', userid)
            new_user.save()
            profile = Profile(user=new_user, tw_uid=userid)
            profile.auth_provider = Profile.TWITTER
            profile.save()

        user = authenticate(username=profile.ujser.username, password=profile.tw_uid)
        login(request, user)
        return JsonResponse({'Auth_complete': 'yes',
                             'access': 'denied'})
    elif social_slug == 'facebook':
        """ Gets the access token and profile info from facebook """
        try:
            code = request.GET.get('code')
        except:
            return JsonResponse({'code': '402'})

        consumer = oauth.Consumer(key=settings.FACEBOOK_APP_ID, secret=settings.FACEBOOK_APP_SECRET)
        client = oauth.Client(consumer)
        redirect_uri = 'http://' + request.META['HTTP_HOST'] + '/connect/facebook/complete/'
        request_url = FACEBOOK_ACCESS_TOKEN_URL + \
                      '?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s' % \
                      (settings.FACEBOOK_APP_ID, redirect_uri, settings.FACEBOOK_APP_SECRET, code)
        resp, content = client.request(request_url, 'GET')
        try:
            access_token = dict(urlparse.parse_qsl(content))['access_token']
        except:
            return JsonResponse({'code': '402', 'message': 'Your link is not valid (old)'})
        request_url = FACEBOOK_CHECK_AUTH + '?access_token=%s' % access_token

        if resp['status'] == '200':
            user_data = get_fb_user_data(access_token)
            userid = user_data['id']
            try:
                profile = Profile.objects.get(fb_uid=userid)
            except:
                name = user_data['name']
                new_user = User.objects.create_user('%s_facebook' % name, user_data['email'], userid)
                new_user.save()
                profile = Profile(user=new_user, fb_uid=userid)
                profile.gender = user_data['gender'][0].lower()
                profile.auth_provider = Profile.FACEBOOK
                profile.save()
            user = authenticate(username=profile.user.username, password=profile.fb_uid)
            login(request, user)
            if profile.gender == Profile.MALE:
                access_granted = True
            else:
                access_granted = False

            if access_granted:
                return JsonResponse({'Auth_complete': 'yes', 'access': 'granted',
                                     'secret': SECRET})
            else:
                return JsonResponse({'Auth_complete': 'yes', 'access': 'denied'})

        else:
            return JsonResponse({'code': '402'})
    elif social_slug == 'google':
        if request.user.is_anonymous():
            the_user = User()
        else:
            the_user = request.user
        try:
            credential = GOOGLE_FLOW_API.step2_exchange(request.GET)
        except:
            return JsonResponse({'code': '402', 'message': 'your link is invalid (old)'})
        http = httplib2.Http()
        http = credential.authorize(http)

        service1 = build('plus', 'v1', http=http)
        service2 = build('gmail', 'v1', http=http)
        name = service1.people().get(userId='me', fields='name').execute().pop('name', None)
        the_user_username = name['familyName'] + '_' + name['givenName']
        same_users = User.objects.filter(username=the_user_username)
        if same_users:
            the_user = same_users[0]
            profile = Profile.objects.get(user=the_user)
        else:
            profile = Profile()

        email = service2.users().getProfile(userId='me', fields='emailAddress').execute()
        the_user.email = email[u'emailAddress']
        the_user.set_password(the_user.email)
        gender = service1.people().get(userId='me', fields='gender').execute()
        name = service1.people().get(userId='me', fields='name').execute().pop('name', None)
        if name:
            the_user.username = name['familyName'] + '_' + name['givenName']
        else:
            the_user.username = the_user.email
        the_user.save()
        gender = gender.pop('gender', None)
        if gender:
            gender = gender[0]
        profile.gender = gender
        profile.user = the_user
        profile.auth_provider = Profile.GOOGLE
        profile.save()
        the_user = authenticate(username=the_user.username, password=the_user.email)
        login(request, the_user)

        if profile.gender == Profile.MALE:
            return JsonResponse({'Auth_complete': 'yes', 'access': 'granted',
                                 'secret': SECRET})
        else:
            return JsonResponse({'Auth_complete': 'yes', 'access': 'denied'})

    else:
        return JsonResponse([{'Bad social network'}])

