# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-20 14:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_auth', '0002_auto_20160620_0842'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='auth_provider',
            field=models.CharField(blank=True, choices=[('fb', 'Facebook'), ('tw', 'Twitter'), ('gg', 'Google')], help_text='Service which was used for registration', max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='gender',
            field=models.CharField(blank=True, choices=[('m', 'Male'), ('f', 'Female')], help_text="User's gender", max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='fb_uid',
            field=models.CharField(blank=True, help_text="User's Facebook ID", max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='tw_uid',
            field=models.CharField(blank=True, help_text="User's Twitter ID", max_length=50, null=True),
        ),
    ]
