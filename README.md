# Startproject
Project, that authenticates you with Twitter, Google or Facebook

## Installation 
Installation requires postgresql database installed.
Database settings are as follow:
```
'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'auth_details',
        'USER': 'test_user',
        'PASSWORD': 'testpassword',
        'HOST': 'localhost',
        'PORT': '', 
    }
```
To run this project on your local machine you need to
1. Git clone.

2.
```
cd startproject
pip install -r'requirements.txt'
sudo python manage.py runserver 80
```
3. Add to your /etc/hosts/
```
127.0.0.1 citirex.com.ua
```

## Usage
You can use any twitter/gmail accout you want, but facebook only allows us to do it with test credentials, which are specified in test_credentials.py
### Authorization
We chose a simple authorization rule: males are to be authorized, females aren't.
This means, you cannot be authrozid by twitter, since it does not provide gender option.
For google gender, google.plus data is used.
If you have no gender there, you will not be authorized.

*We are open to authorization rule change.*

## DDL
Python/Django uses it's own ORM and therefore all SQL is automaticly generated
from social_auth.models.py

Backups are easy with python/django.
You can just do
```
python manage.py dumpdata > 'backup.json'
```

... and you have it. However, we chose to do it on the system level with two scripts, that you can find at /backup/


## API

To use api, at start get
```
citirex.com.ua/api/<twitter|google|facebook>/get_lnik (i.e. /api/twitter/get_link)
```
You will be given a json dict where dict.url is the url, you should visit to tell social network, that you want give access to our application.
Social network will provide you with the corresponding redirect link, which you will need to visit to get authenticated/authorized.

